import axios from 'axios';
import App from "../ui/users/list/List";

class UsersApi {
    constructor() {
        this.baseUrl = 'https://jsonplaceholder.typicode.com';
    }

    create(args) {
        return this.doRequest({
            url: `${this.baseUrl}/users`,
            method: 'post',
            data: JSON.stringify({
                name: args.name,
                username: args.username,
                email: args.email,
            }),
            success: (response) => {
                return response;
            }
        });
    }

    get(userId) {
        return this.doRequest({
            url: `${this.baseUrl}/users/${userId}`,
            success: (user) => {
                return user;
            }
        })
    }

    getAll() {
        return this.doRequest({
            url: `${this.baseUrl}/users`,
            success: (users) => {
                return users;
            }
        });
    }

    update(args) {
        return this.doRequest({
            url: `${this.baseUrl}/users/${args.userId}`,
            method: 'put',
            body: JSON.stringify({
                name: args.name,
                username: args.username,
                email: args.email,
            }),
            success: (response) => {
                return response;
            }
        });
    }

    delete(userId) {
        return this.doRequest({
            url: `${this.baseUrl}/users/${userId}`,
            method: 'delete',
            success: (response) => {
                console.log(response);
            }
        });
    }

    doRequest(args) {
        if (typeof args.url === 'undefined') throw new Error(500, 'URL is not defined!');

        let method = args.method || 'get';
        let data = args.data || {};
        let headers = args.headers || {};

        // Add default Content-type for post and put methods
        if (['put', 'post'].indexOf(method) > -1) {
            Object.assign(headers, {
                "Content-type": "application/json; charset=UTF-8"
            });
        }

        return axios({
            method: method,
            url: args.url,
            data: data,
            headers: headers
        })
            .then(response => {
                return args.success(response.data);
            })
            .catch(error => {
                throw new Error(`Error while making HTTP ${args.method} request to ${args.url}: ${error.toString()}`);
            });
    }
}

export default UsersApi;