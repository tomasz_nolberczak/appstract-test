import React from "react";
import ReactDOM from "react-dom";
import List from "./ui/users/list/List";
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Add from "./ui/users/add/Add";;
import Edit from "./ui/users/edit/Edit";
import Remove from "./ui/users/remove/Remove";

ReactDOM.render(
    <Router>
        <div>
            <Route exact name="list" path="/" component={List}/>
            <Route name="add" path="/add" component={Add}/>
            <Route name="edit" path="/edit/:id" component={Edit}/>
            <Route name="remove" path="/remove/:id" component={Remove}/>
        </div>
    </Router>, document.getElementById("root"));
