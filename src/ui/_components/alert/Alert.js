import React, { Component } from 'react';
import 'bootstrap/scss/bootstrap.scss';
import {Redirect} from "react-router-dom";

class Alert extends Component {
    constructor(props) {
        super(props);

        this.state = {
            close: false
        }
    }
    dismiss() {
        this.setState({
            close: true
        });
    }
    render() {
        if (this.state.close === true) {
            return <Redirect to={{
                pathname: '/',
                state: { message: false }
            }}/>;
        }

        if (this.props.message !== false) {
            return (
                <div className={`alert alert-${this.props.type} alert-dismissible`}>
                    {this.props.message}
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.dismiss.bind(this)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            );
        } else return null;
    }
}

export default Alert;