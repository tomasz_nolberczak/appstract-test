import React, {Component} from 'react';
import UsersApi from "../../../api/UsersApi";
import {Link, Redirect} from "react-router-dom";

class Edit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isReady: false,
            isRedirect: false,
            user: {},
        };
    }

    componentDidMount() {
        const usersApi = new UsersApi();
        const userId = this.getUserIdFromUrl();

        usersApi.get(userId).then(user => {
            this.setState({
                isReady: true,
                user: user
            });
        });
    }

    getUserIdFromUrl() {
        return this.props.match.params.id;
    }

    submitForm(event) {
        event.preventDefault();

        const usersApi = new UsersApi();

        usersApi.update(Object.assign({
            userId: this.getUserIdFromUrl()
        }, this.state.user)).then(() => {
            this.setState({
                isRedirect: true
            });
        });
    }

    onChange(event) {
        let userProfileState = this.state.user;

        userProfileState[event.target.name] = event.target.value;

        this.setState({
            user: userProfileState
        });
    }

    render() {
        const state = this.state;
        const user = this.state.user;

        if (state.isRedirect === true) {
            return <Redirect to={{
                pathname: '/',
                state: { message: 'User successfully saved!' }
            }}/>;
        }

        if (state.isReady === true) {
            return (
                <section id="users-manager" className="edit">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-4">
                                <h2>Edit user</h2>
                                <form action="#" method='post' onSubmit={this.submitForm.bind(this)}>
                                    <div className="form-group">
                                        <label htmlFor="">Name</label>
                                        <input type="text"
                                               className="form-control"
                                               name="name"
                                               value={user.name}
                                               onChange={(event) => this.onChange(event)}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="">Username</label>
                                        <input type="text"
                                               className="form-control"
                                               name="username"
                                               value={user.username}
                                               onChange={(event) => this.onChange(event)}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="">E-mail</label>
                                        <input type="text"
                                               className="form-control"
                                               name="email"
                                               value={user.email}
                                               onChange={(event) => this.onChange(event)}/>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <Link to="/" className="btn btn-light">Back to list</Link>
                                        </div>
                                        <div className="col-md-6">
                                            <button type="submit" className="btn btn-success float-right">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return <section id="users-manager" className="edit">
                <div className="container">
                    <div className="alert alert-info">Loading...</div>
                </div>
            </section>
        }
    }
}

export default Edit;