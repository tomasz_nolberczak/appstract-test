import React, { Component } from 'react';
import 'bootstrap/scss/bootstrap.scss';
import './List.scss';
import UsersApi from '../../../api/UsersApi';
import {Link} from "react-router-dom";
import Alert from "../../_components/alert/Alert";

class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isReady: false,
            users: [],
            message: false
        };
    }
    componentDidMount() {
        this.getUsers();
    }
    getFlashMessage() {
        return typeof this.props.location.state !== "undefined" ? this.props.location.state.message : false;
    }
    getUsers() {
        const usersApi = new UsersApi();

        usersApi.getAll().then(users => {
            this.setState({
                isReady: true,
                users: users
            })
        });
    }
    render() {
        const state = this.state;

        const flashMessage = this.getFlashMessage();

        if (state.isReady === true) {
            if (state.users.length > 0) {
                return <section id="users-manager">
                    <div className="container">
                        <Alert type="info" message={flashMessage}/>
                        <div className="row">
                            <div className="col-md-12">
                                <Link to="add" className="btn btn-primary float-right" id="create-new-user-btn">Create</Link>
                            </div>
                        </div>
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th className="actions">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.users.map((user) => (
                                <tr>
                                    <td>{user.id}</td>
                                    <td>{user.name}</td>
                                    <td>{user.username}</td>
                                    <td>{user.email}</td>
                                    <td className="actions">
                                        <Link to={`/remove/${user.id}`} className="btn btn-danger">
                                            Remove
                                        </Link>
                                        <Link to={`/edit/${user.id}`} className="btn btn-warning">
                                            Edit
                                        </Link>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </section>;
            } else {
                return (
                    <section id="users-manager">
                        <div className="container">
                        <div className="alert alert-info">No users found!</div>
                        </div>
                    </section>)
            }
        } else {
            return (
                <section id="users-manager">
                    <div className="container">
                        <div className="alert alert-info">Loading...</div>
                    </div>
                </section>);
        }
    }
}

export default List;