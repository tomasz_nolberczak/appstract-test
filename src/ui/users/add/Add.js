import React, { Component } from 'react';
import UsersApi from "../../../api/UsersApi";
import {Link, Redirect} from "react-router-dom";

class Add extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isRedirect: false
        };
    }
    submitForm(event) {
        event.preventDefault();

        const usersApi = new UsersApi();

        usersApi.create({
            name: event.currentTarget.querySelector('[name=name]').value,
            username: event.currentTarget.querySelector('[name=nickname]').value,
            email: event.currentTarget.querySelector('[name=email]').value,
        }).then(() => this.setState({
            isRedirect: true
        }));
    }
    render() {
        const state = this.state;

        if (state.isRedirect === true) {
            return <Redirect to={{
                pathname: '/',
                state: { message: 'User successfully created!' }
            }}/>;
        }

        return (
            <section id="users-manager" className="add">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-4">
                            <h2>Create new user</h2>
                            <form action="#" id="form-create-new-user" method='post' onSubmit={this.submitForm.bind(this)}>
                                <div className="form-group">
                                    <label htmlFor="">Name</label>
                                    <input type="text" name="name" className="form-control" required="true"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Username</label>
                                    <input type="text" name="username" className="form-control" required="true"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">E-mail</label>
                                    <input type="text" name="email" className="form-control" required="true"/>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <Link to="/" className="btn btn-light">Back to list</Link>
                                    </div>
                                    <div className="col-md-6">
                                        <button type="submit" className="btn btn-success float-right">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Add;