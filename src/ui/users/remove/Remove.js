import React, {Component} from 'react';
import 'bootstrap/scss/bootstrap.scss';
import './Remove.scss';
import UsersApi from "../../../api/UsersApi";
import {Link, Redirect} from "react-router-dom";

class Remove extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isReady: false,
            isRedirect: false,
            user: {}
        }
    }

    componentDidMount() {
        const usersApi = new UsersApi();
        const userId = this.getUserIdFromUrl();

        usersApi.get(userId).then(user => {
            this.setState({
                isReady: true,
                user: user,
            });
        });
    }

    getUserIdFromUrl() {
        return this.props.match.params.id;
    }

    removeUser(event) {
        event.preventDefault();

        const usersApi = new UsersApi();
        const userId = this.getUserIdFromUrl();

        usersApi.delete(userId).then(() => {
            this.setState({
                isRedirect: true
            })
        });
    }

    render() {
        const state = this.state;

        if (state.isRedirect === true) {
            return <Redirect to={{
                pathname: '/',
                state: { message: 'User successfully deleted!' }
            }}/>;
        }

        if (state.isReady === true) {
            return (
                <section id="users-manager" className="remove">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-4">
                                <form action="#" method="delete" onSubmit={this.removeUser.bind(this)}>
                                    <h2>Are you sure?</h2>
                                    <p>If you click the button, you will remove <strong>{state.user.name}</strong> user data
                                        from database!</p>
                                    <div className="row justify-content-center">
                                        <div className="col-md-4">
                                            <Link to="/" className="btn btn-primary">Back to list</Link>
                                        </div>
                                        <div className="col-md-4">
                                            <button type="submit" className="btn btn-danger">Remove!</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return <section id="users-manager" className="remove">
                <div className="container">
                    <div className="alert alert-info">Loading...</div>
                </div>
            </section>
        }
    }
}

export default Remove;