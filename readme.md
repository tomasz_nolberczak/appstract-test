## Opis zadania

Do zrealizowania było stworzenie prostej aplikacji SPA wyświetlającą listę użytkowników, wraz z obsługą wszystkich akcji CRUD, w oparciu o REST API http://jsonplaceholder.typicode.com/. Zadanie ma zostać wykonane przy użyciu React, React Router v4, Reduxa oraz ES6.
Proste, responsywne widoki na Bootstrapie.
Dodatkowym atutem będzie stworzenie serwisu do zarządzania zapytaniami http, w oparciu o Axios-a, bądź Fetch-a oraz zaproponowanie struktury aplikacji pozwalającej na łatwą skalowalność projektu.

## Uruchomienie aplikacji

Przejdź do katalogu z aplikacją i w terminalu wpisz:

```
npm install
```

a następnie:

```
npm run start
```

Aplikacja jest dostępna pod adresem http://localhost:3000